#include <iostream>
#include "Temperature.h"
#include "Celsius.h"
#include "Fahrenheit.h"

using namespace std;

int main()
{
	Fahrenheit FT;
	Celsius CT;

	cout << "before conversion:" << endl;
	cout << CT << FT << endl;

	Celsius CT_2;
	CT_2 = FT;
	Fahrenheit FT_2;
	FT_2 = CT;

	cout << "after conversion:" << endl;
	cout << CT_2 << FT_2 << endl;

	system("pause");
	return 0;
}