#include "Fahrenheit.h"

Fahrenheit::Fahrenheit()
{
	this->setWaterFreezingPoint(32);
	this->setWaterrBoilingPoint(212);
	this->setAbsoluteZero(-459.67);
}


Fahrenheit::Fahrenheit(Temperature obj)
{
	convertFahrenheitToCelsius(obj);
}

Fahrenheit::~Fahrenheit()
{
}

void Fahrenheit::convertFahrenheitToCelsius(Temperature& obj)
{
	this->setWaterFreezingPoint(0);
	this->setWaterrBoilingPoint(100);
	this->setAbsoluteZero(-273.15);
}

std::ostream& Fahrenheit::print(std::ostream& C_out)
{
	C_out << "Fahrenheit temperature properties are:\n" \
		<< "water boiling point: " << this->getWaterrBoilingPoint() << std::endl \
		<< "water freezing point: " << this->getWaterFreezingPoint() << std::endl \
		<< "absolute zero: " << this->getAbsoluteZero() << std::endl;

	return C_out;
}