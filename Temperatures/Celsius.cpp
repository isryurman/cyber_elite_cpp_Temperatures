#include "Celsius.h"



Celsius::Celsius()
{
	this->setWaterFreezingPoint(0);
	this->setWaterrBoilingPoint(100);
	this->setAbsoluteZero(-273.15);
}

Celsius::Celsius(Temperature obj)
{
	convertCelsiusToFahrenheit(obj);
}

// F=95xC+32 CelsiusToFahrenheit
void Celsius::convertCelsiusToFahrenheit(Temperature& obj)
{
	this->setWaterFreezingPoint(32);
	this->setWaterrBoilingPoint(212);
	this->setAbsoluteZero(-459.67);
}

Celsius::~Celsius()
{
}

std::ostream& Celsius::print(std::ostream& C_out)
{
	C_out << "Celsius temperature properties are:\n" \
		<< "water boiling point: " << this->getWaterrBoilingPoint() << std::endl \
		<< "water freezing point: " << this->getWaterFreezingPoint() << std::endl \
		<< "absolute zero: " << this->getAbsoluteZero() << std::endl;

	return C_out;
}
