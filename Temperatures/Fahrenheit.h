#pragma once
#include "Temperature.h"

class Fahrenheit: public Temperature
{
private:
	void convertFahrenheitToCelsius(Temperature& obj);
	std::ostream& print(std::ostream& C_out);

public:
	Fahrenheit();
	Fahrenheit(Temperature obj);

	~Fahrenheit();
};

