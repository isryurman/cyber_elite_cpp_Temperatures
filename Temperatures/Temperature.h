#pragma once
#include <iostream>

class Temperature
{
private:
	double waterBoilingPoint;
	double waterFreezingPoint;
	double absoluteZero;
	virtual std::ostream& print(std::ostream& C_out) { return C_out; }

public:
	Temperature();
	~Temperature();

	friend std::ostream& operator<< (std::ostream& C_out, Temperature &obj) { return obj.print(C_out); }

	virtual void convert(Temperature&) {}

	// getters
	double getWaterrBoilingPoint();
	double getWaterFreezingPoint();
	double getAbsoluteZero();

	// setters
	void setWaterrBoilingPoint(double);
	void setWaterFreezingPoint(double);
	void setAbsoluteZero(double);
};

