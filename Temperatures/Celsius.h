#pragma once
#include "Temperature.h"

class Celsius: public Temperature
{
private:
	void convertCelsiusToFahrenheit(Temperature& obj);
	std::ostream& print(std::ostream& C_out);

public:
	Celsius();
	Celsius(Temperature obj);

	~Celsius();
};

